(ns fr.grunwald.aquarium.tracker
  (:require [clojure.tools.namespace.track :as track]
            [clojure.tools.namespace.dir :as dir]
            [clojure.tools.namespace.file :as file]
            [clojure.tools.namespace.reload :as rel]
            [clojure.tools.logging.readable :as log]))

(defn new-tracker []
  (track/tracker))

(defn scan-dirs [tracker dirs]
  (dir/scan-dirs tracker dirs))

(defn track-reload-one
  "Executes the next pending unload/reload operation in the dependency
  tracker. Returns the updated dependency tracker. If reloading caused
  an error, it is captured as ::error and the namespace which caused
  the error is ::error-ns."
  [tracker]
  (let [{unload ::track/unload, load ::track/load filemap ::file/filemap} tracker]
    (cond
      (seq unload)
      (let [n (first unload)]
        (rel/remove-lib n)
        (update-in tracker [::track/unload] rest))
      (seq load)
      (let [n (first load)
            file-index (reduce (fn [acc [f n]] (assoc acc n f)) {} filemap)
            f (file-index n)]
        (try (try (load-file (str f)) (catch Exception e (log/warnf e "Error reading %s, skipping..." (str f))))
             (update-in tracker [::track/load] rest)
             (catch Throwable t
               (assoc tracker
                      ::error t ::error-ns n ::track/unload load))))
      :else tracker)))

(defn track-reload
  "Executes all pending unload/reload operations on dependency tracker
  until either an error is encountered or there are no more pending
  operations."
  [tracker]
  (loop [trk (dissoc tracker ::error ::error-ns)]
    (let [{error ::error, unload ::track/unload, load ::track/load} trk]
      (if (and (not error)
               (or (seq load) (seq unload)))
        (recur (track-reload-one trk))
        trk))))

(defn scraper? [n]
  (ns-resolve (symbol (name n)) 'seed))

(defn reload [tracker]
  (log/info "Reloading the following namespaces" (::track/load tracker))
  (let [updated-tracker (track-reload tracker)
        scrapers (reduce (fn [acc [_ n]] (if (scraper? n) (conj acc n) acc)) #{} (::file/filemap updated-tracker))]
    (log/info "Available scrapers" scrapers)
    (assoc updated-tracker :fr.grunwald.aquarium/scrapers scrapers)))
