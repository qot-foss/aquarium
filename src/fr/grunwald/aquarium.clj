(ns fr.grunwald.aquarium
  (:require [silurus.core :as sil]
            [clojure.tools.logging.readable :as log]
            [clojure.string :as str]
            [cheshire.core :as json]
            [omniconf.core :as cfg]
            [unilog.config :as ulog]
            [clojure.pprint :as pprint]
            [silurus.interceptors :as sitcp]
            [clojure.core.async :as as]
            [babashka.fs :as fs]
            [fr.grunwald.aquarium.tracker :as track]
            [fr.grunwald.aquarium.state :as state])
  (:gen-class))

(defn datalevin-storage [{:keys [db-path]}]
  {:uri    (str db-path)
   :engine :silurus.store.datalevin})

(cfg/enable-functions-as-defaults)

(cfg/define
  {:scrapers-path   {:type :directory}
   :scraper-name    {:type :string}
   :logging         {:nested {:file {:type :string}
                              :console {:type :boolean
                                        :default true}
                              :level {:one-of [:trace :debug :info :all :warn :error :off]
                                      :default :info}}}
   :db-path         {:type :directory}
   :output          {:type :directory}
   :action-interval {:type :number
                     :default 2000}
   :max-actions     {:type :number}
   :html-cache-path {:type :directory}})

(def config-merges
  {:db-path #(assoc %1 :store (datalevin-storage {:db-path %2}))
   :html-cache-path (fn [config dir]
                      (if dir
                        (update config :action-chain concat [(sitcp/make-cache-requests-interceptor dir)])
                        config))})

(defn populate-user-config [args]
  (cfg/populate-from-cmd args)
  (when-let [conf (cfg/get :conf)]
    (cfg/populate-from-file conf)))

(defn generate-config [default-config user-config]
  (reduce (fn [acc [k v]]
            (let [f (config-merges k (fn [config v] (assoc config k v)))]
              (f acc v)))
          default-config user-config))

(defn retry-errors [config]
  (let [ctx (as/<!! (sil/start-context config))]
    (sil/retry-action-nodes! ctx (map :ident (sil/error-nodes-list ctx)))))

(defn scrap-status [config]
  (let [ctx (as/<!! (sil/start-context config))
        data (sil/data-nodes-list ctx)
        errors (sil/error-nodes-list ctx)]
    {:data {:sample (take 5 data)
            :count (count data)}
     :errors {:sample (take 5 errors)
              :count (count errors)}}))

(defn dump-scrap-result [config path]
  (let [ctx (as/<!! (sil/start-context config))]
    (log/infof "Dumping scrap results in %s" (str path))
    (sil/dump-scrap-results! ctx path)))

(defn -main
  [op & args]
  (populate-user-config args)
  (cfg/verify)
  (ulog/start-logging! (cfg/get :logging))
  (let [full-config (generate-config {} (cfg/get))]
    (log/debug "Full Configuration" full-config)
    (when-let [path (cfg/get :scrapers-path)]
      (let [tracker (track/scan-dirs (track/new-tracker) [(str path)])
            updated-tracker (track/reload tracker)]
        (reset! state/tracker updated-tracker)))
    (case op
      "list-scrapers" (pprint/pprint (::scrapers @state/tracker))
      "retry-errors" (retry-errors full-config)
      "status" (pprint/pprint (scrap-status full-config))
      "dump" (do (assert (:output full-config))
                 (dump-scrap-result full-config (:output full-config)))
      "scrap" (let [scraper-name (full-config :scraper-name)
                    all-scrapers (into #{} (::scrapers @state/tracker))]
                (assert scraper-name "Need to provide :scraper-name")
                (assert (all-scrapers (symbol scraper-name))
                        (format "Scraper %s is unknown in %s => %s" scraper-name (full-config :scrapers-path) all-scrapers))
                (let [nspace (symbol scraper-name)
                      default-config (deref (ns-resolve nspace 'config))
                      seed (deref (ns-resolve nspace 'seed))
                      merged-config (generate-config default-config full-config)
                      res (as/<!! (sil/start-engine-worker! merged-config (if (fn? seed) (seed) seed)))]
                  (when (instance? Exception res)
                    (throw res))
                  (log/info "Shutting down scraper")
                  (shutdown-agents))))))
